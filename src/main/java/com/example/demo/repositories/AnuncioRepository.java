package com.example.demo.repositories;

import com.example.demo.domain.Anuncio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface AnuncioRepository extends JpaRepository<Anuncio, Long> { }
